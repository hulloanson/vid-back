project:="vid-back"

all: up
	
up: vidbackbase
	docker-compose -p $(project) up --force-recreate --build

down:
	docker-compose -p $(project) down --remove-orphans

vidbackbase:
	docker build -f base/Dockerfile -t vid-back-base .

logsf:
	docker-compose -p $(project) logs -f

logs:
	docker-compose -p $(project) logs
# build:
# 	docker-compose -p $(project) build --no-cache
