from __future__ import unicode_literals
import json
from time import sleep

from time import time
import youtube_dl
# from youtube_dl.extractor import YoutubeYtUserIE

has_error = False
redis_host=None

def scrape():
    with youtube_dl.YoutubeDL({'extract_flat': True}) as e:
        res = e.extract_info('https://youtube.com/user/RTHK/videos',
            # download=False,
        )
        if (res['_type'] != 'playlist'):
            raise Exception('not a playlist')
        return res

def record_titles(res):
    # Send to analyzer instead
    return

run = True

def dumpToJsonfile(scraped_meta, filename='meta.json'):
    with open(filename, 'w+') as f:
        f.write(json.dumps(scraped_meta))

def main():
    dumpToJsonfile(scrape())
    
# def main():
    # while run:
        # record_titles(scrape())
        # sleep(3600)

main()
